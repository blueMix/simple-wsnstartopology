#include "WorldUtilityStats1.h"
#include "Packet.h"
#include "BaseLayer.h"

Define_Module(WorldUtilityStats1);

void WorldUtilityStats1::initialize(int stage)
{
    BaseWorldUtility::initialize(stage);
    if (stage == 0) {
        recordVectors = par("recordVectors");
        bitrate = par("bitrate");

        bitsSent = 0;
        bitsReceived = 0;

        subscribe(BaseLayer::catPacketSignal, this);

        sent.setName("Bits generated");
        rcvd.setName("Bits received");
    }
}

void WorldUtilityStats1::receiveSignal(cComponent *source, simsignal_t signalID, cObject *obj)
{
    Enter_Method_Silent();

    if (signalID == BaseLayer::catPacketSignal){
        const Packet *pkt = static_cast<const Packet*>(obj);
        double nBitsSent = pkt->getNbBitsSent();
        double nBitsRcvd = pkt->getNbPacketsReceived();

        bitsSent += nBitsSent;
        bitsReceived += nBitsRcvd;

        if (recordVectors){
            sent.record(bitsSent);
            rcvd.record(bitsReceived);
        }
    }
}

void WorldUtilityStats1::finish()
{
    recordScalar("GlobalTrafficGenerated", bitsSent, "bit");
    recordScalar("GlobalTrafficReceived", bitsReceived, "bit");

    recordScalar("Traffic", bitsSent/bitrate/simTime());

    double hosts = simulation.getSystemModule()->par("numHosts");
    if (! par("bcTraffic")) {
        hosts = 2;
    }

    recordScalar("Usage", bitsReceived/bitrate/simTime()/(hosts-1));

}
