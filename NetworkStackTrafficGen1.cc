//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//

#include "NetworkStackTrafficGen1.h"

#include <cassert>

#include "Packet.h"
#include "BaseMacLayer.h"
#include "FindModule.h"
#include "NetwToMacControlInfo.h"

Define_Module(NetworkStackTrafficGen1);

void NetworkStackTrafficGen1::initialize(int stage)
{
    flag = true;
    cModule *c = getParentModule();
    EV << "getFullName(): " << c->getFullName() << endl;

    //int nic = c->findSubmodule("nic");
    //if (nic != -1) ev << "nic submodule found!" << endl;

    ev << "Iterating over submodules: " << endl;
    for (cModule::SubmoduleIterator i(c); !i.end(); i++) {
        ev << i()->getFullName() << " ";
    }

    ev << endl << endl;
    ev << "stage: " << stage << endl;
    ev << c->getFullName() << " - Iterating over nic module gates: " << endl;
    for (cModule::GateIterator i(c->getSubmodule("nic")); !i.end(); i++) {
        cGate *gate = i();
        //cGate *otherGate = gate->getType()==cGate::INPUT ? gate->getNextGate() : gate->getPreviousGate();

        /*if (otherGate)
            ev << "######## otherGate is connected to: " << otherGate->getFullPath() << endl;
        else
            ev << "######## otherGate not connected" << endl;*/


        ev << gate->getFullName() << ": ";
        ev << "id=" << gate->getId() << ", ";

        if (!gate->isVector())
            ev << "scalar gate, ";
        else
            ev << "gate " << gate->getIndex() << " in vector "
                    << gate->getName() << " of size " << gate->getVectorSize()
                    << ", ";
        ev << "type:" << cGate::getTypeName(gate->getType());
        ev << "\n";

        cGate *nextGate = gate->getNextGate();
        cGate *prevGate = gate->getPreviousGate();

        if (nextGate) EV <<"nextGate: " << nextGate->getFullPath() << endl;
        if (prevGate) EV <<"prevGate: " << prevGate->getFullPath() << endl;
    }

    ev << endl << endl;

    EV << "Entering initialize..." << endl;

	BaseLayer::initialize(stage);

	if(stage == 0) {
		world        = FindModule<BaseWorldUtility*>::findGlobalModule();
		delayTimer   = new cMessage("delay-timer", SEND_BROADCAST_TIMER);

		//arp          = FindModule<BaseArp*>::findSubModule(findHost());
		//myNetwAddr   = arp->myNetwAddr(this);

		myNetwAddr     = LAddress::L3Type(par("myNetwAddr").longValue());
		EV << "myNetwAddr: " << myNetwAddr << endl;

		packetLength = par("packetLength");
		packetTime   = par("packetTime");
		pppt         = par("packetsPerPacketTime");
		burstSize    = par("burstSize");
		destination  = LAddress::L3Type(par("destination").longValue());

		nbPacketDropped = 0;
	} else if (stage == 1) {
		if(burstSize > 0) {
			remainingBurst = burstSize;
			scheduleAt(dblrand() * packetTime * burstSize / pppt, delayTimer);
		}
	} else {

	}
}

NetworkStackTrafficGen1::~NetworkStackTrafficGen1() {
	cancelAndDelete(delayTimer);
}


void NetworkStackTrafficGen1::finish()
{
	recordScalar("dropped", nbPacketDropped);
}

void NetworkStackTrafficGen1::handleSelfMsg(cMessage *msg)
{


	switch( msg->getKind() )
	{
	case SEND_BROADCAST_TIMER:
		assert(msg == delayTimer);

		//LAddress::L3Type dest2;
		//dest2 = dynamic_cast<NetwPkt*>(msg)->getDestAddr();
		//if (static_cast<NetwPkt*>(msg)->getDestAddr() == destination)

	    if (myNetwAddr == destination && ev.isGUI()) {
	        ev << "the central node!" << endl;
	        bubble("the central node!");
	        return;
	    }

		sendBroadcast();

		remainingBurst--;

		if(remainingBurst == 0) {
			remainingBurst = burstSize;
			scheduleAt(simTime() + (dblrand()*1.4+0.3)*packetTime * burstSize / pppt, msg);
		} else {
			scheduleAt(simTime() + packetTime * 2, msg);
		}

		break;
	default:
		EV << "Unkown selfmessage! -> delete, kind: "<<msg->getKind() <<endl;
		delete msg;
		break;
	}
}


void NetworkStackTrafficGen1::handleLowerMsg(cMessage *msg)
{
    //EV << "handleLowerMsg - msg->getKind: " << msg->getKind() << endl;
    //EV << "BROADCAST_MESSAGE: " << BROADCAST_MESSAGE << endl;

	Packet p(packetLength, 1, 0);
	emit(BaseMacLayer::catPacketSignal, &p);

    LAddress::L3Type src_ = static_cast<NetwPkt*>(msg)->getSrcAddr();
    EV << "handleLowerMsg - src_: " << src_ << endl;

	delete msg;
	msg = 0;
}


void NetworkStackTrafficGen1::handleLowerControl(cMessage *msg)
{
	if(msg->getKind() == BaseMacLayer::PACKET_DROPPED) {
		nbPacketDropped++;
	}

	delete msg;
	msg = 0;
}

void NetworkStackTrafficGen1::sendBroadcast()
{
	NetwPkt *pkt = new NetwPkt("BROADCAST_MESSAGE", BROADCAST_MESSAGE);
	pkt->setBitLength(packetLength);

	pkt->setSrcAddr(myNetwAddr);
	pkt->setDestAddr(destination);

	NetwToMacControlInfo::setControlInfo(pkt, LAddress::L2BROADCAST);

	Packet p(packetLength, 0, 1);
	emit(BaseMacLayer::catPacketSignal, &p);

	    sendDown(pkt);
}

