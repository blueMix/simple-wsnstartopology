//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __WSNTEST_NetworkStackTrafficGen1_H_
#define __WSNTEST_NetworkStackTrafficGen1_H_

#include <omnetpp.h>
#include "NetwPkt_m.h"
#include "SimpleAddress.h"
#include "BaseLayer.h"
#include "BaseArp.h"
#include "BaseWorldUtility.h"


class NetworkStackTrafficGen1 : public BaseLayer
{
public:
    enum TrafficGenMessageKinds {
        SEND_BROADCAST_TIMER = 1,
        BROADCAST_MESSAGE
    };

protected:
    int packetLength;
    simtime_t packetTime;
    double pppt;
    int burstSize;
    int remainingBurst;
    LAddress::L3Type destination;

    long nbPacketDropped;

    BaseArp* arp;
    LAddress::L3Type myNetwAddr;

    cMessage *delayTimer;
    BaseWorldUtility *world;

    bool flag;

public:
    virtual ~NetworkStackTrafficGen1();
    virtual void initialize(int stage);
    virtual void finish();

protected:
    virtual void handleSelfMsg(cMessage *msg);
    virtual void handleLowerMsg(cMessage *msg);
    virtual void handleLowerControl(cMessage *msg);

    virtual void handleUpperMsg(cMessage *msg)
    {
        opp_error("NetworkStackTrafficGen1 has no upper layers!");
        delete msg;
    }

    virtual void handleUpperControl(cMessage *msg)
    {
        opp_error("NetworkStackTrafficGen1 has no upper layers!");
        delete msg;
    }

    virtual void sendBroadcast();
};

#endif
