/*
 * WorldUtilityStats1.h
 *
 *  Created on: Feb 23, 2014
 *      Author: bluemix
 */

#ifndef WORLDUTILITYSTATS_H_
#define WORLDUTILITYSTATS_H_

#include <omnetpp.h>
#include "BaseWorldUtility.h"

class WorldUtilityStats1 : public BaseWorldUtility,
                          public cListener
{
protected:
    double bitsSent;
    double bitsReceived;

    cOutVector sent, rcvd;

    bool recordVectors;

    double bitrate;

protected:
    virtual void initialize(int stage);

public:
    virtual void receiveSignal(cComponent *source, simsignal_t signalID, cObject *obj);
    virtual void finish();
};



#endif /* WORLDUTILITYSTATS_H_ */
